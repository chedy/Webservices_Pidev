package tn.irmc.esprit.entities;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToMany;
import javax.persistence.ManyToOne;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
@Entity
public class Document implements Serializable {
	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
    private int id ; 
    private String name ;
    private String description ;
    private String pieceJointe ; 
    private String image ; 
    private Boolean etat ; 
    @Enumerated(EnumType.STRING)
    private Category category ; 
    private int rate ; 
    @Temporal(TemporalType.DATE)
    private Date postDate ; 
    private int searchDate ; 
    
    
    @ManyToOne
    private User user ;


	public int getId() {
		return id;
	}


	public void setId(int id) {
		this.id = id;
	}


	public String getName() {
		return name;
	}


	public void setName(String name) {
		this.name = name;
	}


	public String getDescription() {
		return description;
	}


	public void setDescription(String description) {
		this.description = description;
	}


	public String getPieceJointe() {
		return pieceJointe;
	}


	public void setPieceJointe(String pieceJointe) {
		this.pieceJointe = pieceJointe;
	}


	public String getImage() {
		return image;
	}


	public void setImage(String image) {
		this.image = image;
	}


	public Boolean getEtat() {
		return etat;
	}


	public void setEtat(Boolean etat) {
		this.etat = etat;
	}


	public Category getCategory() {
		return category;
	}


	public void setCategory(Category category) {
		this.category = category;
	}


	public int getRate() {
		return rate;
	}


	public void setRate(int rate) {
		this.rate = rate;
	}


	public Date getPostDate() {
		return postDate;
	}


	public void setPostDate(Date postDate) {
		this.postDate = postDate;
	}


	public int getSearchDate() {
		return searchDate;
	}


	public void setSearchDate(int searchDate) {
		this.searchDate = searchDate;
	}


	public User getUser() {
		return user;
	}


	public void setUser(User user) {
		this.user = user;
	}


	@Override
	public String toString() {
		return "Document [name=" + name + ", description=" + description + ", pieceJointe=" + pieceJointe + ", image="
				+ image + ", etat=" + etat + ", category=" + category + ", rate=" + rate + ", postDate=" + postDate
				+ ", searchDate=" + searchDate + ", user=" + user + "]";
	}


	public Document(String name, String description, String pieceJointe, String image, Boolean etat, Category category,
			int rate, Date postDate, int searchDate, User user) {
		super();
		this.name = name;
		this.description = description;
		this.pieceJointe = pieceJointe;
		this.image = image;
		this.etat = etat;
		this.category = category;
		this.rate = rate;
		this.postDate = postDate;
		this.searchDate = searchDate;
		this.user = user;
	}


	public Document(int id, String name, String description, String pieceJointe, String image, Boolean etat,
			Category category, int rate, Date postDate, int searchDate, User user) {
		super();
		this.id = id;
		this.name = name;
		this.description = description;
		this.pieceJointe = pieceJointe;
		this.image = image;
		this.etat = etat;
		this.category = category;
		this.rate = rate;
		this.postDate = postDate;
		this.searchDate = searchDate;
		this.user = user;
	}


	public Document() {
		super();
		// TODO Auto-generated constructor stub
	}


	public Document(int id, String name, String description) {
		super();
		this.id = id;
		this.name = name;
		this.description = description;
	} 
	
    
    
    
      
      
      
      
      
      
 
}
