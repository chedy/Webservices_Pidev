package tn.irmc.esprit.entities;

import java.io.Serializable;
import java.util.Date;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

@Entity
public class User implements Serializable{

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private int id ; 
	private String lastName ; 
	private String firstName ; 
	@Temporal(TemporalType.DATE)
	private Date birthDate ; 
	private String country ; 
	private String city ; 
	private String address ; 
	private String email ; 
	private long phoneNumber ; 
	private String login ; 
	private String password ;  
	@Enumerated(EnumType.STRING)
	private  Role role ;
	private String photo;
	
	@OneToMany(mappedBy = "user", cascade = CascadeType.MERGE, fetch=FetchType.EAGER)
	private List<Offre> offres;
	
	@OneToMany(mappedBy = "user", cascade = CascadeType.MERGE, fetch=FetchType.EAGER)
	private List<Evenement> events;
	
	@OneToMany(mappedBy = "user", cascade = CascadeType.MERGE, fetch=FetchType.EAGER)
	private List<Reclamation> Reclamation;
	
	@OneToMany(mappedBy = "user", cascade = CascadeType.MERGE, fetch=FetchType.EAGER)
	private List<Favoris> Favoris;
	
	@OneToMany(mappedBy = "user", cascade = CascadeType.MERGE, fetch=FetchType.EAGER)
	private List<CentreDoc> centres;
	
	@OneToMany(mappedBy="user" ,fetch = FetchType.EAGER)
	private List<Document> documents ; 
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	
	public String getLastName() {
		return lastName;
	}
	public void setLastName(String lastName) {
		this.lastName = lastName;
	}
	public String getFirstName() {
		return firstName;
	}
	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}
	public Date getBirthDate() {
		return birthDate;
	}
	public void setBirthDate(Date birthDate) {
		this.birthDate = birthDate;
	}
	public String getCountry() {
		return country;
	}
	public void setCountry(String country) {
		this.country = country;
	}
	public String getCity() {
		return city;
	}
	public void setCity(String city) {
		this.city = city;
	}
	public String getAddress() {
		return address;
	}
	public void setAddress(String adress) {
		this.address = adress;
	}
	public String getEmail() {
		return email;
	}
	public void setEmail(String email) {
		this.email = email;
	}
	public long getPhoneNumber() {
		return phoneNumber;
	}
	public void setPhoneNumber(long phoneNumber) {
		this.phoneNumber = phoneNumber;
	}
	public String getLogin() {
		return login;
	}
	public void setLogin(String login) {
		this.login = login;
	}
	public String getPassword() {
		return password;
	}
	public void setPassword(String password) {
		this.password = password;
	}
	
	public String getPhoto() {
		return photo;
	}
	public void setPhoto(String photo) {
		this.photo = photo;
	}
	public List<Evenement> getEvents() {
		return events;
	}
	public void setEvents(List<Evenement> events) {
		this.events = events;
	}
	public List<CentreDoc> getCentres() {
		return centres;
	}
	public void setCentres(List<CentreDoc> centres) {
		this.centres = centres;
	}
	public List<Offre> getOffres() {
		return offres;
	}
	public void setOffres(List<Offre> offres) {
		this.offres = offres;
	}
	public List<Document> getDocuments() {
		return documents;
	}
	public void setDocuments(List<Document> documents) {
		this.documents = documents;
	}
	
		
	public List<Reclamation> getReclamation() {
		return Reclamation;
	}
	public void setReclamation(List<Reclamation> reclamation) {
		Reclamation = reclamation;
	}
	public User() {
		super();
		// TODO Auto-generated constructor stub
	}
	public User(String email, String login, String password) {
		super();
		this.email = email;
		this.login = login;
		this.password = password;
	}
	public User(String lastName, String firstName, Date birthDate, String country, String city, String address,
			String email, long phoneNumber, String login, String password, String photo) {
		super();
		this.lastName = lastName;
		this.firstName = firstName;
		this.birthDate = birthDate;
		this.country = country;
		this.city = city;
		this.address = address;
		this.email = email;
		this.phoneNumber = phoneNumber;
		this.login = login;
		this.password = password;
		this.photo = photo;
	}
	@Override
	public String toString() {
		return "User [id=" + id + ", lastName=" + lastName + ", firstName=" + firstName + ", birthDate=" + birthDate
				+ ", country=" + country + ", city=" + city + ", address=" + address + ", email=" + email
				+ ", phoneNumber=" + phoneNumber + ", login=" + login + ", password=" + password + ", photo=" + photo
				+ "]";
	}
	public Role getRole() {
		return role;
	}
	public void setRole(Role role) {
		this.role = role;
	} 
	
	
	
	
		
	
	
	
	
	
	
}
