package tn.irmc.esprit.entities;

import java.io.Serializable;
import javax.persistence.*;

/**
 * Entity implementation class for Entity: Favoris
 *
 */
@Entity

public class Favoris implements Serializable {

	   
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	private int idFavoris;
	@ManyToOne
	private User user;
	@ManyToOne
	private Offre offre;
	public User getUser() {
		return user;
	}
	public void setUser(User user) {
		this.user = user;
	}
	public Offre getOffre() {
		return offre;
	}
	public void setOffre(Offre offre) {
		this.offre = offre;
	}

	private static final long serialVersionUID = 1L;

	public Favoris() {
		super();
	}   
	public int getIdFavoris() {
		return this.idFavoris;
	}

	public void setIdFavoris(int idFavoris) {
		this.idFavoris = idFavoris;
	}
   
}
