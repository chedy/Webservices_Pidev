package tn.irmc.esprit.entities;

import java.io.Serializable;
import java.util.Date;
import java.util.List;

import javax.persistence.*;

/**
 * Entity implementation class for Entity: Offre
 *
 */
@Entity
public class Offre implements Serializable {

	   
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	private int id;
	@ManyToOne
	private User user;
	private String TypeOffre;
	private Date DatePublication;
	private Date DateFin;
	private String Titre;
	private String DescriptionBourse;
	private String ConditionBourse;
	private String PieceBourse;
	private String Reference;
	private String TypePoste;
	private String Mobilite;
	private String Experience;
	private String Disponibilite;
	private String Langue;
	private String Entreprise;
	private String Contexte;
	private String Profil;
	private int salaire;
	private String societé;
	private boolean etat;
	private String domain;
	private String numeroTel;
	private String Email;
	private String Adresse;
	@OneToMany(mappedBy = "offre", cascade = CascadeType.REMOVE, fetch=FetchType.EAGER)
	private List<Reclamation> Reclamation;
	
	@OneToMany(mappedBy = "offre", cascade = CascadeType.REMOVE, fetch=FetchType.EAGER)
	private List<Favoris> Favoris;
	
	



	public Offre(User user, String titre, String reference, String typePoste, String mobilite, String experience,
			String disponibilite, String langue, String entreprise, String contexte, String profil) {
		super();
		this.user = user;
		Titre = titre;
		Reference = reference;
		TypePoste = typePoste;
		Mobilite = mobilite;
		Experience = experience;
		Disponibilite = disponibilite;
		Langue = langue;
		Entreprise = entreprise;
		Contexte = contexte;
		Profil = profil;
	}
	

	
	public Offre(int id, String titre, String reference, String typePoste,
			String mobilite, String experience, String disponibilite, String langue, String entreprise, String contexte,
			String profil, int salaire, String societé) {
		super();
		this.id = id;
		Titre = titre;
		Reference = reference;
		TypePoste = typePoste;
		Mobilite = mobilite;
		Experience = experience;
		Disponibilite = disponibilite;
		Langue = langue;
		Entreprise = entreprise;
		Contexte = contexte;
		Profil = profil;
		this.salaire = salaire;
		this.societé = societé;
	}


	public Offre( String titre, String reference, String typePoste, String mobilite, String experience,
			String disponibilite, String langue, String entreprise, String contexte, String profil ,int sal, String soc) {
		super();
		Titre = titre;
		Reference = reference;
		TypePoste = typePoste;
		Mobilite = mobilite;
		Experience = experience;
		Disponibilite = disponibilite;
		Langue = langue;
		Entreprise = entreprise;
		Contexte = contexte;
		Profil = profil;
		salaire =sal;
		societé=soc;
	}

	public int getId() {
		return id;
	}

	public int getSalaire() {
		return salaire;
	}

	public void setSalaire(int salaire) {
		this.salaire = salaire;
	}

	public String getSocieté() {
		return societé;
	}

	public void setSocieté(String societé) {
		this.societé = societé;
	}

	public void setId(int id) {
		this.id = id;
	}

	public User getUser() {
		return user;
	}

	public void setUser(User user) {
		this.user = user;
	}

	public String getDomain() {
		return domain;
	}


	public void setDomain(String domain) {
		this.domain = domain;
	}


	public String getNumeroTel() {
		return numeroTel;
	}


	public void setNumeroTel(String numeroTel) {
		this.numeroTel = numeroTel;
	}


	public String getEmail() {
		return Email;
	}


	public void setEmail(String email) {
		Email = email;
	}


	public String getAdresse() {
		return Adresse;
	}


	public void setAdresse(String adresse) {
		Adresse = adresse;
	}


	@Temporal(TemporalType.DATE)
	public Date getDateFin() {
		return DateFin;
	}
	public void setDateFin(Date dateFin) {
		DateFin = dateFin;
	}
	public String getTitre() {
		return Titre;
	}
	public void setTitre(String titre) {
		Titre = titre;
	}
	public String getDescriptionBourse() {
		return DescriptionBourse;
	}
	public void setDescriptionBourse(String descriptionBourse) {
		DescriptionBourse = descriptionBourse;
	}
	public String getConditionBourse() {
		return ConditionBourse;
	}
	public void setConditionBourse(String conditionBourse) {
		ConditionBourse = conditionBourse;
	}
	public String getPieceBourse() {
		return PieceBourse;
	}
	public void setPieceBourse(String pieceBourse) {
		PieceBourse = pieceBourse;
	}
	public String getReference() {
		return Reference;
	}
	public void setReference(String reference) {
		Reference = reference;
	}
	public String getTypePoste() {
		return TypePoste;
	}
	public void setTypePoste(String typePoste) {
		TypePoste = typePoste;
	}
	public String getMobilite() {
		return Mobilite;
	}
	public void setMobilite(String mobilite) {
		Mobilite = mobilite;
	}
	public String getExperience() {
		return Experience;
	}
	public void setExperience(String experience) {
		Experience = experience;
	}
	public String getDisponibilite() {
		return Disponibilite;
	}
	public void setDisponibilite(String disponibilite) {
		Disponibilite = disponibilite;
	}
	public String getLangue() {
		return Langue;
	}
	public void setLangue(String langue) {
		Langue = langue;
	}
	public String getEntreprise() {
		return Entreprise;
	}
	public void setEntreprise(String entreprise) {
		Entreprise = entreprise;
	}
	public String getContexte() {
		return Contexte;
	}
	public void setContexte(String contexte) {
		Contexte = contexte;
	}
	public String getProfil() {
		return Profil;
	}
	public void setProfil(String profil) {
		Profil = profil;
	}

	private static final long serialVersionUID = 1L;

	public Offre() {
		super();
	}   
	public int getIdOffre() {
		return this.id;
	}

	public void setIdOffre(int idOffre) {
		this.id = idOffre;
	}   
	public String getTypeOffre() {
		return this.TypeOffre;
	}

	public void setTypeOffre(String TypeOffre) {
		this.TypeOffre = TypeOffre;
	}   
	public Date getDatePublication() {
		return this.DatePublication;
	}

	public void setDatePublication(Date DatePublication) {
		this.DatePublication = DatePublication;
	}


	public boolean isEtat() {
		return etat;
	}


	public void setEtat(boolean etat) {
		this.etat = etat;
	}
	
	public List<Favoris> getFavoris() {
		return Favoris;
	}



	public void setFavoris(List<Favoris> favoris) {
		Favoris = favoris;
	}
   
}
