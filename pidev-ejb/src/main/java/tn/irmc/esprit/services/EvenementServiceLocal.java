package tn.irmc.esprit.services;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.sql.Time;
import java.util.Date;
import java.util.List;

import javax.ejb.Local;


import tn.irmc.esprit.entities.Document;
import tn.irmc.esprit.entities.Evenement;
import tn.irmc.esprit.entities.TypeOfEvents;
import tn.irmc.esprit.entities.User;

@Local
public interface EvenementServiceLocal {
	void addEvent(Evenement evenement);
	void updateEvent(Evenement evenement);
	void removeEvent(Evenement evenement);
	List<Evenement>findAllEvents();

	 Evenement findById(int idEvent);
	  Evenement findByName(String nom);
	  User findByIdUser(int id);
	  
	  
	  //Metiers

	  List<Evenement>search(String nom,String lieu,String map);
	   List<Evenement> getByType(String type);
	   List<Evenement>search(String  Recherche);
	   
	   int groupedByTheme(TypeOfEvents theme);
	  

}
