package tn.irmc.esprit.services;

import java.util.List;   
import java.util.logging.Level;
import java.util.logging.Logger;

import javax.ejb.LocalBean;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.TypedQuery;


import tn.irmc.esprit.entities.CentreDoc;
import tn.irmc.esprit.entities.CurrentUser;
import tn.irmc.esprit.entities.Localisation;
import tn.irmc.esprit.entities.User;;

/**
 * Session Bean implementation class CentreService
 */
@Stateless
@LocalBean
public class CentreService implements CentreServiceRemote, CentreServiceLocal {

    /**
     * Default constructor. 
     */
    public CentreService() {
        // TODO Auto-generated constructor stub
    }
    @PersistenceContext
	EntityManager em;
	
	
	public static int IDC;
	
	
	//**********************************************************************************
		   
	
		
		@Override
		public void addCentre(CentreDoc centre, Localisation local) {
			// TODO Auto-generated method stub
			//centre.setUser(findByIdUser(CurrentUser.CurrentUserId));
			em.persist(centre);
			local.setCentredoc(centre);
			em.persist(local);
		}
		
		@Override
		public void UpdateCentre(CentreDoc centre) {
			em.merge(centre);	
		}
		
		@Override
		public List<CentreDoc> getCentres() {
			
			TypedQuery<CentreDoc> querry=em.createQuery("select c from CentreDoc c",CentreDoc.class);
			return querry.getResultList();
		}

		
		@Override
		public void DeleteCentre(CentreDoc centre, Localisation local,int cenID) {
			// TODO Auto-generated method stub
			
			centre=em.find(CentreDoc.class, cenID);
			local.setCentredoc(centre);
			em.remove(local);
			em.remove(em.merge(centre));
			
		}

//*******************************************************************************************
		@Override
		public User findByIdUser(int id) {
			// TODO Auto-generated method stub
			return em.find(User.class, id);
		}

//******************************************************************************************
		@Override
		public CentreDoc findById(int idCentre) {
			// TODO Auto-generated method stub
		
			return em.find(CentreDoc.class, idCentre); 	
		}


		@Override
		public List<CentreDoc> findAllCentreDocs() {
			// TODO Auto-generated method stub
			return null;
		}


		@Override
		public CentreDoc findByName(String name) {
			CentreDoc found = null;
			TypedQuery<CentreDoc> query = em.createQuery(
					"select e from CentreDoc e where e.cen_nom=:x", CentreDoc.class);
			query.setParameter("x", name);
			try {
				found = query.getSingleResult();
			} catch (Exception ex) {
				Logger.getLogger(this.getClass().getName()).log(Level.INFO,
						"no Centre de documentation with name=" + name);
			}
			System.out.println(name);
			return found;
		}
		
		@Override
		public List<CentreDoc> findConfirmedCentre() {
			return em.createQuery("SELECT a FROM CentreDoc a  JOIN a.localisation b WHERE  a.confirmed = 1   ",CentreDoc.class).getResultList();

			}
		
		@Override
		public List<CentreDoc> findByUserCountry() {
			User uid = findByIdUser(CurrentUser.CurrentUserId);
			String city =uid.getCity();
			TypedQuery<CentreDoc> query = em.createQuery("SELECT a FROM CentreDoc a   WHERE a.localisation.cen_ville = :city ",CentreDoc.class);
			
			query.setParameter("city", city);
			query.setFirstResult(0);
			query.setMaxResults(3);
			return query.getResultList();
			
			
			
			
			
			
			
			
			
			
			
			
		}
//***********************************************************************************************
		@Override
		public void confirmCentre(CentreDoc centre) {
			// TODO Auto-generated method stub
		centre.setConfirmed(1);	
		}

	//******************************************************RECHERCHE 5 CRITERES*********************************************
		 TypedQuery<CentreDoc>query ;
		@Override
		public List<CentreDoc> RechercheCentre(String nomSigle , String pays, String ville,String type,int recom ) {
		
		
			 if(nomSigle.contains(nomSigle) && pays.isEmpty() && ville.isEmpty() && type.isEmpty()&& recom == 0)
			{
				 
					
					TypedQuery<CentreDoc> query = em.createQuery("SELECT c FROM CentreDoc c WHERE c.cen_nom = :param",CentreDoc.class);
					query.setParameter("param", nomSigle);
					System.out.println ("NOOOOOOOOOOOOOOMMMM OOOOOOOOOOKKKKK");
					return query.getResultList();
				
			}
			 else if(pays.isEmpty() && ville.contains(ville) && nomSigle.isEmpty() && type.isEmpty()&& recom == 0)
				{
					TypedQuery<CentreDoc> query = em.createQuery("SELECT c FROM CentreDoc c WHERE c.localisation.cen_ville  =:param",CentreDoc.class);
					
					query.setParameter("param", ville);
					
					return query.getResultList();
				}
				else if(pays.isEmpty() && ville.isEmpty() && nomSigle.isEmpty() && type.contains(type)&& recom == 0)
				{
					TypedQuery<CentreDoc> query = em.createQuery("SELECT c FROM CentreDoc c WHERE c.Type = :param",CentreDoc.class);
					
					
					query.setParameter("param", type);
					
					return query.getResultList();
				}
				else if(pays.isEmpty() && ville.isEmpty() && nomSigle.isEmpty() && type.isEmpty()&& recom >= 0)
				{
					TypedQuery<CentreDoc> query = em.createQuery("SELECT c FROM CentreDoc c WHERE c.recommandation >=  :param",CentreDoc.class);
					
					
					query.setParameter("param", recom);
					
					return query.getResultList();
				}
			else if (pays.contains(pays) && nomSigle.isEmpty() && ville.isEmpty() && type.isEmpty()&& recom == 0)
			{
				TypedQuery<CentreDoc> query = em.createQuery("SELECT c FROM CentreDoc c JOIN c.localisation l where c.cen_id = l.centredoc and c.localisation.cen_pays =:param",CentreDoc.class);
				query.setParameter("param", pays);
				
				System.out.println ("PAYYYYYYYYYYYYYYSSSSSSSS  oooookkkkkkkkk");
				return query.getResultList();
			}
				
			else if(pays.isEmpty() && ville.isEmpty() && nomSigle.isEmpty() && type.contains(type)&& recom >= 0)
			{
				TypedQuery<CentreDoc> query = em.createQuery("SELECT c FROM CentreDoc c WHERE c.Type like :param AND c.recommandation >= :parame",CentreDoc.class);
				
				
				query.setParameter("param", "%"+type+"%");
				query.setParameter("parame", recom);
				return query.getResultList();	
			}
			
			
			
			
			else if(pays.isEmpty() && ville.contains(ville) && nomSigle.isEmpty() && type.contains(type)&& recom == 0)
			{
				TypedQuery<CentreDoc> query = em.createQuery("SELECT c FROM CentreDoc c WHERE c.localisation.cen_ville like :param AND c.Type like :parame",CentreDoc.class);
				
				
				query.setParameter("param", "%"+ville+"%");
				query.setParameter("parame", "%"+type+"%");
				return query.getResultList();	
			}
			else if(pays.isEmpty() && ville.contains(ville) && nomSigle.isEmpty() && type.isEmpty()&& recom >= 0)
			{
				TypedQuery<CentreDoc> query = em.createQuery("SELECT c FROM CentreDoc c WHERE c.localisation.cen_ville like :param AND c.recommandation >= :parame",CentreDoc.class); 
				
				
				query.setParameter("param", "%"+ville+"%");
				query.setParameter("parame", recom);
				return query.getResultList();	
				
			}
			else if(pays.contains(pays) && ville.isEmpty() && nomSigle.isEmpty() && type.isEmpty()&& recom >= 0)
			{	
				TypedQuery<CentreDoc> query = em.createQuery("SELECT c FROM CentreDoc c WHERE c.localisation.cen_pays like :param AND c.recommandation >= :parame",CentreDoc.class);
				
				query.setParameter("param", "%"+pays+"%");
				query.setParameter("parame", recom);
				return query.getResultList();	
			}
			else if (pays.contains(pays) && ville.contains(ville) && nomSigle.isEmpty() && type.isEmpty()&& recom == 0)
				{
				TypedQuery<CentreDoc> query = em.createQuery("SELECT c FROM CentreDoc c WHERE c.localisation.cen_pays = :param AND c.localisation.cen_ville = :parame",CentreDoc.class);
				query.setParameter("param", pays);
				query.setParameter("parame", ville);
				System.out.println ("PAYYYYYYYYYYYYYYSSSSSSSS ville oooookkkkkkkkk");
				return query.getResultList();
				
				}
			
			
		
			else if(pays.contains(pays) && ville.isEmpty() && nomSigle.isEmpty() && type.contains(type)&& recom == 0)
			{
				
				TypedQuery<CentreDoc> query = em.createQuery("SELECT c FROM CentreDoc c WHERE c.localisation.cen_pays = :param AND c.Type = :parame",CentreDoc.class);
				
				System.out.println ("PAYYYYYYYYYYYYYYSSSSSSSS  TYPE     oooookkkkkkkkk");
				query.setParameter("param", pays);
				query.setParameter("parame", type);
				return query.getResultList();
				
				
			}
		
			else if(pays.contains(pays) && ville.contains(ville) && nomSigle.isEmpty() && type.contains(type)&& recom == 0)
			{ 
				
				TypedQuery<CentreDoc> query = em.createQuery("SELECT c FROM CentreDoc c WHERE c.localisation.cen_pays = :param AND c.localisation.cen_ville = :parame1 AND c.Type = :parame2",CentreDoc.class);
				System.out.println("pays ville type OOOOOKKK");
				query.setParameter("param", pays);
				query.setParameter("parame1", ville);
				query.setParameter("parame2", type);
				return query.getResultList();
			}
			
		
			else if(pays.contains(pays) && ville.contains(ville) && nomSigle.isEmpty() && type.isEmpty()&& recom >= 0)
			{
				TypedQuery<CentreDoc> query = em.createQuery("SELECT c FROM CentreDoc c WHERE c.localisation.cen_pays like :param AND c.localisation.cen_ville like :parame1 AND c.recommandation >= :parame2",CentreDoc.class);
				
				
				query.setParameter("param", "%"+pays+"%");
				query.setParameter("parame1", "%"+ville+"%");
				query.setParameter("parame2", recom);
				return query.getResultList();
			}
			 
			else if(pays.contains(pays) && ville.isEmpty() && nomSigle.isEmpty() && type.contains(type)&& recom >= 0)
			{
				TypedQuery<CentreDoc> query = em.createQuery("SELECT c FROM CentreDoc c WHERE c.localisation.cen_pays like :param AND c.Type like :parame1 AND c.recommandation >= :parame2",CentreDoc.class);
				
				
				query.setParameter("param", "%"+pays+"%");
				query.setParameter("parame1", "%"+type+"%");
				query.setParameter("parame2", recom);
				return query.getResultList();
				
			}
		
		
			else if(pays.isEmpty() && ville.contains(ville) && nomSigle.isEmpty() && type.contains(type)&& recom >= 0)
			{	TypedQuery<CentreDoc> query = em.createQuery("SELECT c FROM CentreDoc c WHERE c.localisation.cen_ville like :param AND c.Type like :parame1 AND c.recommandation >= :parame2",CentreDoc.class);
			
				
				query.setParameter("param", "%"+ville+"%");
				query.setParameter("parame1", "%"+type+"%");
				query.setParameter("parame2", recom);
				return query.getResultList();
				
			}
			else if(pays.contains(pays) && ville.contains(ville) && nomSigle.isEmpty() && type.contains(type)&& recom >= 0)
			{
				TypedQuery<CentreDoc> query = em.createQuery("SELECT c FROM CentreDoc c WHERE c.localisation.cen_pays like :param AND c.Type like :parame1  AND c.recommandation >= :parame2 AND c.localisation.cen_ville like :parame3",CentreDoc.class);
				query.setParameter("param", "%"+pays+"%");
				query.setParameter("parame1", "%"+type+"%");
				query.setParameter("parame2", recom);
				query.setParameter("parame3", "%"+ville+"%");
				return query.getResultList();
				
			}
			
			
			
			 return null;
				}
					
   //************************************************************************************************************************

/*
		@Override
		public List<CentreDoc> RechercheCentrePaysVille(String pays, String ville) {
System.out.println ("3333333333333333333");
			
			TypedQuery<CentreDoc> query = em.createQuery("SELECT c FROM CentreDoc c WHERE c.localisation.cen_pays = :param AND c.localisation.cen_ville = :parame",CentreDoc.class);
			query.setParameter("param", pays);
			query.setParameter("parame", ville);
			return query.getResultList();
		}

	*/

		


		
		
		

}
