package tn.irmc.esprit.services;

import java.util.List;

import javax.ejb.LocalBean;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import tn.irmc.esprit.entities.CurrentUser;
import tn.irmc.esprit.entities.Favoris;
import tn.irmc.esprit.entities.Reclamation;
import tn.irmc.esprit.entities.User;

/**
 * Session Bean implementation class FavorisService
 */
@Stateless
@LocalBean
public class FavorisService implements FavorisServiceRemote, FavorisServiceLocal {

	@PersistenceContext
	private EntityManager em;
    /**
     * Default constructor. 
     */
    public FavorisService() {
        // TODO Auto-generated constructor stub
    }

	@Override
	public void MarkFav(Favoris fav) {
		fav.setUser(findByIdUser(CurrentUser.CurrentUserId));
		em.persist(fav);
		
	}

	@Override
	public void UnmarkFav(Favoris fav) {
		
		em.remove(em.merge(fav));
		
	}

	@Override
	public Favoris findById(int idFav) {
		
		return em.find(Favoris.class, idFav);
	}

	@Override
	public List<Favoris> findAllFav() {
		
		return em.createQuery("SELECT f FROM Favoris f ", Favoris.class)
				.getResultList();
	}
	
	
    


	
	public User findByIdUser(int id) {
		// TODO Auto-generated method stub
		return em.find(User.class, id);
	}

	

}
