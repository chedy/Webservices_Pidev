package tn.irmc.esprit.services;

import java.util.List;

import javax.ejb.Remote;

import tn.irmc.esprit.entities.CentreDoc;
import tn.irmc.esprit.entities.Localisation;
import tn.irmc.esprit.entities.User;

@Remote
public interface CentreServiceRemote {
	void addCentre(CentreDoc centre , Localisation local);
	//void DeleteCentre(int id);
	//void DeleteCentre(int id);
	void DeleteCentre(CentreDoc centre, Localisation local,int cenID);
    void UpdateCentre(CentreDoc centre);
	public List<CentreDoc> findByUserCountry ();

    
	
	
	List<CentreDoc> getCentres();
	
	

}
