package tn.irmc.esprit.services;

import java.util.ArrayList;
import java.util.List;

import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;

import tn.irmc.esprit.entities.Category;
import tn.irmc.esprit.entities.Document;

@Stateless
public class DocumentService implements DocumentServiceRemote {

	@PersistenceContext EntityManager em ; 
	
	@Override
	public int addDocument(Document document) {
		em.persist(document);
		return document.getId();
	}

	@Override
	public List<Document> getAllDocuments() {
		Query req = em.createQuery("Select d from Document d"); 
		
		return req.getResultList();
	}

	@Override
	public Boolean deleteDocument(int idDoc) {      
          	Document d = em.find(Document.class, idDoc);
          	if(d.getRate() <  5 ) {
          		
          		em.remove(d);
          		return true;
          	}
          	
          	return false ; 
          	 
	}

	@Override
	public void  updateDocument(Document document) {
		
		
		em.merge(document);
		
		
	}

	@Override
	public List<Document> getDocumentsByCategory(Category category) {
		List<Document> lst = new ArrayList<Document>();
		   
		
			Query req = em.createQuery("SELECT s From Document s WHERE s.category =:category"); 
		    req.setParameter("category", category);
			
			lst =  req.getResultList();
		
			
		
		
		return lst ; 
		
	}

	@Override
	public List<Document> getTopRated() {
		Query req = em.createQuery("Select d From Document d ORDER BY d.rate Desc");
		return req.getResultList();
	}

	@Override
	public List<Document> search(String motCle) {
		 Query req = em.createQuery("SELECT s From Document s where s.name LIKE  CONCAT('%',:motCle,'%') or s.description LIKE CONCAT('%',:motCle,'%') or s.category LIKE CONCAT('%',:motCle,'%') ").setParameter("motCle",motCle); 
			return req.getResultList();
	}

	@Override
	public List<Document> getMyDocument(int idUser) {
	      Query req = em.createQuery("Select d From Document d WHERE d.user.id =:id "); 
	      req.setParameter("id", idUser);
		return req.getResultList();
	}

	@Override
	public List<Document> getByDate() {
		Query req = em.createQuery("Select d From Document d ORDER BY d.searchDate Desc");
		return req.getResultList();
	}

	@Override
	public Document getDetails(int idDoc) {
		Query req = em.createQuery("Select d From Document d where d.id =:id") ; 
		req.setParameter("id", idDoc); 
		
		return (Document) req.getSingleResult();
	}

	@Override
	public void rateDocument(int idDoc) {
		Document d = em.find(Document.class,idDoc); 
		d.setRate(d.getRate()+1);
		em.merge(d);
		
	}

}
