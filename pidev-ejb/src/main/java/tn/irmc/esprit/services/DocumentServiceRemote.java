package tn.irmc.esprit.services;

import java.util.List;

import javax.ejb.Remote;

import tn.irmc.esprit.entities.Category;
import tn.irmc.esprit.entities.Document;

@Remote
public interface DocumentServiceRemote {
   //Crud 
	public int addDocument(Document document); 
	public List<Document> getAllDocuments(); 
	public Boolean  deleteDocument(int idDoc); 
	public void updateDocument(Document document); 
	//Partie metiier 
	public List<Document> getDocumentsByCategory(Category category);
	public List<Document> getTopRated();
	public List<Document> search(String motCle);
	public List<Document> getMyDocument(int idUser);
	public List<Document> getByDate();
	
	public Document getDetails(int idDoc) ; 
	
	public void rateDocument(int idDoc);
	
    
	
}
