package tn.irmc.esprit.services;

import java.util.List;

import javax.ejb.Local;

import tn.irmc.esprit.entities.User;

@Local
public interface UserServiceLocal {
	
void createUser(User user);
	
	List<User> findAllUsers();
	User authenticate(String login, String password);
	boolean loginExists(String login);
	User findUserByLogin(String login);
	void UpdateUser(User user);
	void DeleteUser(User user);
    User findById(int iduser);

}
