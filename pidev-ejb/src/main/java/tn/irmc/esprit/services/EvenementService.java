package tn.irmc.esprit.services;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.sql.Time;
import java.util.Date;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.lang.Object;


import javax.ejb.LocalBean;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;
import javax.persistence.TypedQuery;


import tn.irmc.esprit.entities.CurrentUser;
import tn.irmc.esprit.entities.Document;
import tn.irmc.esprit.entities.Evenement;
import tn.irmc.esprit.entities.TypeOfEvents;
import tn.irmc.esprit.entities.User;

/**
 * Session Bean implementation class CRUDevent
 */
@Stateless
@LocalBean
public class EvenementService implements EvenementServiceLocal,EvenementServiceRemote {
	
	@PersistenceContext
	private EntityManager em;
	
	UserServiceLocal serviceUserServiceLocal;
	
public static int ESL;

    /**
     * Default constructor. 
     */
    public EvenementService() {
        // TODO Auto-generated constructor stub
    }
    
   

    
    @Override
	public void addEvent(Evenement evenement) {
		// TODO Auto-generated method stub
    	evenement.setUser(findByIdUser(CurrentUser.CurrentUserId));	
	    em.persist(evenement);
	}
    @Override
	public User findByIdUser(int id) {
		// TODO Auto-generated method stub
		return em.find(User.class, id);
	}
	

	@Override
	public void updateEvent(Evenement evenement) {
		//evenement.setUser(findByIdUser(CurrentUser.CurrentUserId));
		em.merge(evenement);
		
	}

	@Override
	public void removeEvent(Evenement evenement) {
		// TODO Auto-generated method stub
		//evenement.setUser(findByIdUser(CurrentUser.CurrentUserId));
		em.remove(em.merge(evenement));
		
	}

	@Override
	public List<Evenement> findAllEvents() {
		// TODO Auto-generated method stub
		return em.createQuery("select e from Evenement e", Evenement.class)
				.getResultList();
	}

	@Override
	public Evenement findById(int idEvent) {
		// TODO Auto-generated method stub
		
		 return em.find(Evenement.class, idEvent);
	}

	@Override
	public Evenement findByName(String nom) {
		// TODO Auto-generated method stub
		Evenement found = null;
		TypedQuery<Evenement> query = em.createQuery(
				"select e from Evenement e where e.Nom=:x", Evenement.class);
		query.setParameter("x", nom);
		try {
			found = query.getSingleResult();
		} catch (Exception ex) {
			Logger.getLogger(this.getClass().getName()).log(Level.INFO,
					"no Evenement with name=" + nom);
		}
		return found;
	}




	@Override
	public List<Evenement> search(String nom, String lieu, String map) {
		if(nom!= null && lieu!=null)
		{
			String jpql="SELECT e FROM Evenement e WHERE e.Nom like :param and e.Lieux like :parame and e.Map like :paramee and type='Séminaire'";
			javax.persistence.Query query =em.createQuery(jpql);
			query.setParameter("param", "%"+nom+"%");
			query.setParameter("parame", "%"+lieu+"%");
			query.setParameter("paramee", "%"+map+"%");
			return  query.getResultList();
			
		}
		if(nom!= null )
		{
			String jpql="SELECT e FROM Evenement e WHERE e.Nom like :param  and type='Séminaire'";
			javax.persistence.Query query =em.createQuery(jpql);
			query.setParameter("param", "%"+nom+"%");
		
			return  query.getResultList();
			
		}
		if( lieu!=null)
		{
			String jpql="SELECT e FROM Evenement e WHERE  e.Lieux like :param and type='Séminaire'";
			javax.persistence.Query query =em.createQuery(jpql);
			query.setParameter("param", "%"+lieu+"%");
			return  query.getResultList();
			
		}
		if( map!=null)
		{
			String jpql="SELECT e FROM Evenement e WHERE  e.Map like :param and type='Séminaire'";
			javax.persistence.Query query =em.createQuery(jpql);
			query.setParameter("param", "%"+map+"%");
			return  query.getResultList();
			
		}
		return null;
	}




	@Override
	public List<Evenement> getByType(String type) {
		Query jpql = em.createQuery("Select e From Evenement e WHERE e.type like :param");
		jpql.setParameter("param", "%"+type+"%");
		
		return jpql.getResultList();
		
		
	}




	@Override
	public List<Evenement> search(String Recherche) {
		
		
	return em.createQuery("SELECT e FROM Evenement e WHERE e.Nom "
			+ "like :param OR e.Lieux like :param OR e.Map like :param OR e.type like :param",Evenement.class)
			.setParameter("param","%"+Recherche+"%" ).getResultList();	
	
	}
	@Override
	public int groupedByTheme(TypeOfEvents type) {
		
		TypedQuery<Long> query = em.createQuery(
			      "SELECT count(*) FROM Evenement e where e.type=:x", Long.class).setParameter("x", type);
			 Long countryCount = query.getSingleResult();
			 System.out.println(countryCount);
			 return countryCount.intValue();
				
	}


	



	

}

