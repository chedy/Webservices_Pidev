package tn.irmc.esprit.services;

import java.util.List;

import javax.ejb.Local;

import tn.irmc.esprit.entities.Favoris;;

@Local
public interface FavorisServiceLocal {
	
	public void MarkFav(Favoris fav);
	public void UnmarkFav(Favoris fav);
	public Favoris findById(int idFav);
	public List<Favoris> findAllFav();

}

	
