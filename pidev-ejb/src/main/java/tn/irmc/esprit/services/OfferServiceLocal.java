package tn.irmc.esprit.services;

import java.util.List;

import javax.ejb.Local;

import tn.irmc.esprit.entities.Offre;
import tn.irmc.esprit.entities.User;

@Local
public interface OfferServiceLocal {

	public void addoffre(Offre item);
	public void updateOffre(Offre item); 
	public void deleteOffre(Offre item);
	public Offre findById(int idItem);
	public List<Offre> findAllOffers();
	public Offre findByName(String name);
	public User findByIdUser(int id);
	public List<Offre> findMyAllOffersAttente();
	public void addBourse(Offre item);
	public void updateBourse(Offre item); 
	public void deleteBourse(Offre item);
	public List<Offre> findAllBourse();
	public List<Offre> findMyAllBourseAttente();
	public List<Offre> findMyAllAttente();
	public List<Offre> findMyOffrePub();
	public List<Offre> findMyBoursePub();
	public List<Offre> findAllOffreExpiré();
	public List<Offre> findAllOffreAttente();
	public List<Offre> findAllbourseAttente();
	public void deleteAttente();
	public void AccepteAttente(int id);
	public void RefuserAttente(int id);
	public List<Offre> RechercheOffre(String title , String lieu);
	public List<Offre> RechercheBourse(String title , String type);
	

}
