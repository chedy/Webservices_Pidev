package tn.irmc.esprit.services;

import java.util.List;

import javax.ejb.Remote;

import tn.irmc.esprit.entities.Favoris;

@Remote
public interface FavorisServiceRemote {
	public void MarkFav(Favoris fav);
	public void UnmarkFav(Favoris fav);
	public Favoris findById(int idFav);
	public List<Favoris> findAllFav();

}
