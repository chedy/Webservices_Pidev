package tn.irmc.esprit.services;

import java.sql.Time;
import java.util.Date;
import java.util.List;

import javax.ejb.Remote;

import tn.irmc.esprit.entities.Evenement;
import tn.irmc.esprit.entities.User;

@Remote
public interface EvenementServiceRemote {

	void addEvent(Evenement evenement);
	void updateEvent(Evenement evenement);
	void removeEvent(Evenement evenement);
	List<Evenement>findAllEvents();
	 Evenement findById(int idEvent);
	  Evenement findByName(String nom);


}
