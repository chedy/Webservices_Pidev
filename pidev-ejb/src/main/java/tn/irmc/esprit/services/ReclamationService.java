package tn.irmc.esprit.services;

import java.util.List;

import javax.ejb.LocalBean;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import tn.irmc.esprit.entities.CurrentUser;
import tn.irmc.esprit.entities.Offre;
import tn.irmc.esprit.entities.Reclamation;
import tn.irmc.esprit.entities.User;

/**
 * Session Bean implementation class ReclamationService
 */
@Stateless
@LocalBean
public class ReclamationService implements ReclamationServiceRemote, ReclamationServiceLocal {

    /**
     * Default constructor. 
     */
	@PersistenceContext
	private EntityManager em;
    public ReclamationService() {
        // TODO Auto-generated constructor stub
    }

	@Override
	public void addReclamation(Reclamation item) {
		// TODO Auto-generated method stub
		item.setUser(findByIdUser(CurrentUser.CurrentUserId));
		em.persist(item);
	}

	@Override
	public void deleteReclamation(Reclamation item) {
		// TODO Auto-generated method stub
		em.remove(em.merge(item));
	}

	@Override
	public Reclamation findById(int idItem) {
		// TODO Auto-generated method stub
		return em.find(Reclamation.class, idItem);
	}
	public User findByIdUser(int id) {
		// TODO Auto-generated method stub
		return em.find(User.class, id);
	}

	@Override
	public List<Reclamation> findAllRec() {
		// TODO Auto-generated method stub
		return em.createQuery("SELECT i FROM Reclamation i ", Reclamation.class)
				.getResultList();
	}
}
