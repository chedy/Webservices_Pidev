package tn.irmc.esprit.WServices;
import java.util.ArrayList;
import java.util.List;

import javax.ejb.EJB;
import javax.ejb.Remove;
import javax.enterprise.context.RequestScoped;
import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.Response.Status;

import tn.irmc.esprit.entities.Offre;
import tn.irmc.esprit.entities.Reclamation;
import tn.irmc.esprit.services.OfferServiceLocal;
import tn.irmc.esprit.services.ReclamationsServiceLocal;

@RequestScoped
@Path("reclamations")
public class ReclamationResources {
	@EJB
    static ReclamationsServiceLocal recService;

	@Consumes(MediaType.APPLICATION_JSON)
	@POST
	public Response addReclamation(@QueryParam("url")int ref,Reclamation r) {
		recService.addReclamation(r,ref);
		return Response.status(Status.CREATED).build();
	}
	}
