package tn.irmc.esprit.WServices;

import java.util.ArrayList;
import java.util.List;

import javax.ejb.EJB;
import javax.ejb.Remove;
import javax.enterprise.context.RequestScoped;
import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.Response.Status;

import tn.irmc.esprit.entities.Offre;
import tn.irmc.esprit.services.OfferServiceLocal;

@RequestScoped
@Path("offres")
public class WebServiceOfferRessources {
	@EJB
    static OfferServiceLocal serviceOffer;

	@GET
	@Produces(MediaType.APPLICATION_JSON)
	public Response getAlloffre(@QueryParam("url") String myList ) {
		if(myList!=null)	
			return Response.ok(serviceOffer.findMyOffrePub()).build();		
		return Response.ok(serviceOffer.findAllOffers()).build();
	}
	@Consumes(MediaType.APPLICATION_JSON)
	@POST
	public Response addEmployee(Offre o) {
		serviceOffer.addoffre(o);
		return Response.status(Status.CREATED).build();
	}
	@PUT
	@Consumes(MediaType.APPLICATION_JSON)
	public Response modifierRendezVous(Offre o) {
		serviceOffer.updateOffre(o);
		return Response.status(Status.OK).entity(o).build();
	}

	/*@GET
	@Produces(MediaType.APPLICATION_JSON)
	public Response getoffmyoffre() {
		return Response.ok(serviceOffer.findMyOffrePub()).build();
	}*/

	@DELETE
	@Consumes(MediaType.APPLICATION_JSON)
	public Response remove(Offre o) {
		
		serviceOffer.deleteOffre(o);
		return Response.status(Status.OK).build();
	}
}
