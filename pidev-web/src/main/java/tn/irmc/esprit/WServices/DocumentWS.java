package tn.irmc.esprit.WServices;

import javax.ejb.EJB;
import javax.enterprise.context.RequestScoped;
import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.Response.Status;

import tn.irmc.esprit.entities.Document;
import tn.irmc.esprit.services.DocumentServiceRemote;

@Path("documents")
@RequestScoped
public class DocumentWS {
  @EJB
  DocumentServiceRemote docService ; 
  
 
  @GET
  @Produces(MediaType.APPLICATION_JSON)
  public Response getDocuments(){
	 if(docService.getAllDocuments().isEmpty() == false ){
      return Response.ok(docService.getAllDocuments()).build();
      }
	 return Response.status(Status.NOT_FOUND).entity("There Is No Documents Available Right Now , Try Again Later").build() ; 
  }
  
  @POST
  @Consumes(MediaType.APPLICATION_JSON) 
  @Produces("application/json")
  public Response addDocument(Document d){
	  int test = docService.addDocument(d);
	   if(test != 0) {
	  return Response.status(Status.ACCEPTED).entity(test).build();
	   }
	   return Response.status(Status.NOT_ACCEPTABLE).entity("Please Try Again An Error Has Occured !").build() ; 
			    }
  
  @DELETE
  @Produces("application/json")
  @Path("{id}")
  public Response deleteDocument(@PathParam("id") int idDoc){
	 
	  return Response.status(Status.OK).entity(docService.deleteDocument(idDoc)).build();
  }
  }
