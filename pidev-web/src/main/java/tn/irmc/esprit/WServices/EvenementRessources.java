package tn.irmc.esprit.WServices;



import javax.ejb.EJB;
import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.Response.Status;
import javax.enterprise.context.RequestScoped;
import tn.irmc.esprit.entities.Evenement;
import tn.irmc.esprit.services.EvenementServiceLocal;



@Path("events")
@RequestScoped

public class EvenementRessources {

	
	@EJB
	private EvenementServiceLocal evenementServiceLocal;
	
	@POST
	@Consumes(MediaType.APPLICATION_JSON)
	
	public Response createevent(Evenement ev) {
		
		evenementServiceLocal.addEvent(ev);
		return Response.status(200).entity("Event Created").build();
	}

	@GET
	@Produces(MediaType.APPLICATION_JSON)
	public Response getAllEvents(){
		
		
		return Response.ok(evenementServiceLocal.findAllEvents()).build();
		
	}
	@Path("{id}")
	@DELETE
	@Consumes(MediaType.APPLICATION_JSON)
	public Response deleteById(@PathParam(value = "id") int id) {
		Evenement e=new Evenement();
	e=evenementServiceLocal.findById(id);
		
		evenementServiceLocal.removeEvent(e);

		return Response.status(Status.OK).build();
		}
	
	@Path("/{id}")
	@PUT
	public Response updateeventById(@PathParam(value = "id") int id,Evenement r) {
		
Evenement t = new Evenement();
	t=	evenementServiceLocal.findById(id);
	t=r;
	t.setNom("kaisser");
	
	

	evenementServiceLocal.updateEvent(t);
		return Response.status(Status.OK).build();
		
	}
	@Path("/search/{id}")
	@GET
	@Produces(MediaType.APPLICATION_JSON)
	public Response findeventbyname(@PathParam(value = "id") int  id) {
		//evenementServiceLocal.findById(id);
		return Response.status(200).entity(evenementServiceLocal.findById(id)).build();
	
	}	
	
	

	}
	
	
	


