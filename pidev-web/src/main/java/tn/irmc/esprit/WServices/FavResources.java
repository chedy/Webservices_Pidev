package tn.irmc.esprit.WServices;

import javax.ejb.EJB;
import javax.enterprise.context.RequestScoped;
import javax.ws.rs.DELETE;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.Response.Status;

import tn.irmc.esprit.entities.Favoris;
import tn.irmc.esprit.entities.User;
import tn.irmc.esprit.services.FavorisServiceLocal;

@RequestScoped
@Path("favs")
public class FavResources {
	
	@EJB
	private FavorisServiceLocal FavServ;
	
	//******************MarkFav (No Ok)******************//
	
	@POST
	public Response AddFavoris(Favoris fav){
		
		FavServ.MarkFav(fav);
		return Response.ok("Favoris marked").build();
	}
	
	//******************* Unmark Favoris **********************//
	
	@DELETE
	@Path("{id}")
	public Response deleteFavoris(@PathParam("id") int id) {
		FavServ.UnmarkFav(FavServ.findById(id));
		return Response.status(Status.OK).entity("favoris retired from the list").build();
	}

}
