package tn.irmc.esprit.WServices;

import java.util.List;
import javax.ejb.EJB;
import javax.enterprise.context.RequestScoped;
import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.Response.Status;

import tn.irmc.esprit.entities.User;
import tn.irmc.esprit.services.UserServiceLocal;

@Path("users")
@RequestScoped
public class UserResources {

	@EJB
	private UserServiceLocal userServ;

	// ******************* Ajout **************************//

	@POST
	@Consumes(MediaType.APPLICATION_JSON)
	public Response addReclamation(User user) {
		userServ.createUser(user);
		return Response.status(Status.CREATED).entity("User added").build();

	}

	// **********************Affichage******************************//

	@GET
	@Produces(MediaType.APPLICATION_JSON)
	public Response GetAllUsers() {

		List<User> All = userServ.findAllUsers();

		if (All.isEmpty()) {
			return Response.noContent().build();
		} else
			return Response.ok(All).build();
	}

	@GET
	@Path("{id}")
	@Produces(MediaType.APPLICATION_JSON)
	public Response SearchUserById(@PathParam("id") int id) {

		User user = this.userServ.findById(id);
		if (user == null)
			return Response.status(Status.NOT_FOUND).build();
		else
			return Response.ok(user).build();
	}

	// ******************* Modification (Non Ok)**************************//
	@PUT
	@Consumes(MediaType.APPLICATION_JSON)
	@Path("{id}")
	public Response ModifyUser(@PathParam("id") int id) {

		User user = this.userServ.findById(id);
		if (user == null)
			return Response.status(Status.NOT_FOUND).build();
		else
			userServ.UpdateUser(user);
		return Response.status(Status.OK).entity("User modified successfully").build();

	}

	@GET
	@Path("auth")
	@Produces(MediaType.APPLICATION_JSON)
	public Response LogIn(@QueryParam("l") String login, @QueryParam("p") String password) {

		User user = new User();
		user = userServ.authenticate(login, password);
		return Response.ok(user).build();
	}

	// ******************* Suppression **************************//

	@DELETE
	@Path("{id}")
	public Response deleteUser(@PathParam("id") int id) {
		userServ.DeleteUser(userServ.findById(id));
		return Response.status(Status.OK).entity("User Removed from database").build();
	}

	/*
	 * @DELETE
	 * 
	 * @Consumes(MediaType.APPLICATION_JSON) public Response Delete(User user) {
	 * 
	 * userServ.DeleteUser(user); return Response.ok().build(); }
	 */

}
