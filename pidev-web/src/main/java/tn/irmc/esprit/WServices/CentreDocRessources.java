package tn.irmc.esprit.WServices;

import java.util.ArrayList; 
import java.util.List;

import javax.ejb.EJB;
import javax.enterprise.context.RequestScoped;
import javax.servlet.annotation.HttpMethodConstraint;
import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.Response.Status;

import tn.irmc.esprit.entities.CentreDoc;
import tn.irmc.esprit.services.CentreService;
import tn.irmc.esprit.services.CentreServiceLocal;

@Path("centres")
@RequestScoped
public class CentreDocRessources {
	
	@EJB
	private CentreServiceLocal centreService;
	
	@GET
	@Produces(MediaType.APPLICATION_JSON)
	public Response ListCentres(){
		
		
		
		return Response.ok(centreService.getCentres()).build();
	}
	
	@GET
	@Produces(MediaType.APPLICATION_JSON)
	@Path("{nom}")
	public Response ListCentres1(@PathParam("nom")String nom){
		
		
		
		return Response.ok(centreService.RechercheCentreNom(nom)).build();
	}
	
	@DELETE
	@Consumes(MediaType.APPLICATION_JSON)
	@Path("/delete/{id}")
	public Response  deleteById(@PathParam("id")int id){
		centreService.DeleteCentre(id);
		return Response.status(Status.OK).entity("centre delete").build();
	}
	
	@POST
	@Consumes(MediaType.APPLICATION_JSON)
	public Response addCentre(CentreDoc centredoc) {
	   String s = centredoc.getCen_nom();
	   if (centredoc.getCen_nom().isEmpty()) {
		   return Response.status(Status.OK).build();
	   }
	   else {  centreService.addCentre(centredoc);
		return Response.status(Status.CREATED).entity("Centre proposé avec succès \n nous allons traiter votre proposition plus tard ").build();
		  
		   
	   }
	   
	}
	

}
