package tn.irmc.esprit.ManagedBeans;

import javax.faces.bean.ApplicationScoped;
import javax.faces.bean.ManagedBean;

import tn.irmc.esprit.entities.Category;
import tn.irmc.esprit.entities.TypeOfEvents;

@ManagedBean
@ApplicationScoped
public class Data {
  
	public Category[] getCategory(){
		return Category.values(); 
		
	}
	
	public TypeOfEvents[] getTypeOfEvents(){
		return TypeOfEvents.values();
	}
}
