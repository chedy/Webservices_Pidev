package tn.irmc.esprit.ManagedBeans;

import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.io.Serializable;
import java.nio.file.Files;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import javax.annotation.PostConstruct;
import javax.ejb.EJB;
import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.RequestScoped;
import javax.faces.context.FacesContext;

import javax.servlet.http.Part;



import tn.irmc.esprit.entities.CurrentUser;
import tn.irmc.esprit.entities.Evenement;
import tn.irmc.esprit.services.EvenementServiceLocal;

@ManagedBean
// @SessionScoped
@RequestScoped
public class EvenementBean implements Serializable {

	/**
	 * 
	 */

	private static final long serialVersionUID = 1L;
	@EJB
	private EvenementServiceLocal evenementServiceLocal;

	// private Part file;
	private String destination = "http://localhost:8012/";

	private Evenement e;
	private List<Evenement> events;
	private List<Evenement> myevent = new ArrayList<>();
	private List<Evenement> foundEvent = new ArrayList<Evenement>();
	private List<Evenement> foundByType = new ArrayList<Evenement>();
	private List<Evenement> foundByNomUtili = new ArrayList<Evenement>();
	private boolean showDetails = false;
	private Date date;

	private static String motRecherche;

	private Part file;
	




	public EvenementBean() {

		// TODO Auto-generated constructor stub
	}

	@PostConstruct
	public void Init() throws Exception {
		
		e = new Evenement();
		events = evenementServiceLocal.findAllEvents();
		myevent = evenementServiceLocal.findAllEvents();

		date = new Date();
		
	

	}
	// add

	public String doAddEvent() throws Exception {
		String navTo = "/pages/User/EventsList?faces-redirect=true";

		e.setImg(destination + file.getSubmittedFileName());

		evenementServiceLocal.addEvent(e);
		this.uploadFile(file.getSubmittedFileName());

		// Init();
		return navTo;

	}

	// select
	public void doSelect() {
		// String navTo="/pages/User/MyListEvent?faces-redirect=true";
		showDetails = true;
		// return navTo;
	}

	// update
	public String edit(Evenement ee) {
		this.e = ee;
		return "editEvent";
	}

	// update
	public String edit() throws IOException, Exception {

		// this.e.getUser();
		// this.e.setNom("kaisser");
		
		this.e.setUser(evenementServiceLocal.findByIdUser(CurrentUser.CurrentUserId));
		this.e.setNom(e.getNom());
		this.e.setDateDebut(e.getDateDebut());
		this.e.setDateFin(e.getDateFin());
		this.e.setTel(e.getTel());
		this.e.setNbrPlace(e.getNbrPlace());
		this.e.setLieux(e.getLieux());
		this.e.setMap(e.getMap());
		this.e.setType(e.getType());
		
		
		this.e.setDescription(e.getDescription());
		this.e.setImg(destination + file.getSubmittedFileName());
		
		this.evenementServiceLocal.updateEvent(this.e);
		this.uploadFile(file.getSubmittedFileName());
		

		return "/pages/User/EventsList?faces-redirect=true";
	}

	// delete
	public String doDelete() {
		String navTo = "/pages/User/EventsList?faces-redirect=true";
		evenementServiceLocal.removeEvent(e);
		events = evenementServiceLocal.findAllEvents();
		// Init();
		return navTo;
	}

	// search by three (Nom,Lieu,localisation)
	public void doSearch() {
	
		myevent = evenementServiceLocal.search(motRecherche);

	}

	// search by type
	public String doSearchByType() {
		String navTo = "/pages/User/SearchbyType?faces-redirect=true";

		return navTo;

	}

	// captcha
	public void submit() {
		FacesMessage msg = new FacesMessage(FacesMessage.SEVERITY_INFO, "Correct", "Correct");
		FacesContext.getCurrentInstance().addMessage(null, msg);
	}


	

	public void uploadFile(String nom) throws IOException, Exception {
		InputStream input = new InputStream() {

			@Override
			public int read() throws IOException {
				// TODO Auto-generated method stub
				return 0;
			}
		};

		input = file.getInputStream();
		Files.copy(input, new File("c:/xampp/htdocs/", nom).toPath());
	}

	public String doBack() {
		String navTo = "/pages/User/MyListEvent?faces-redirect=true";
		return navTo;
	}
	
	
	
	

	     

	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	

	public List<Evenement> getEvents() {
		return events;
	}

	public void setEvents(List<Evenement> events) {
		this.events = events;
	}

	public boolean isShowDetails() {
		return showDetails;
	}

	public void setShowDetails(boolean showDetails) {
		this.showDetails = showDetails;
	}

	public Evenement getE() {
		return e;
	}

	public void setE(Evenement e) {
		this.e = e;
	}

	public List<Evenement> getMyevent() {
		return myevent;
	}

	public void setMyevent(List<Evenement> myevent) {
		this.myevent = myevent;
	}

	public Date getDate() {
		return date;
	}

	public void setDate(Date date) {
		this.date = date;
	}

	public List<Evenement> getFoundEvent() {
		return foundEvent;
	}

	public void setFoundEvent(List<Evenement> foundEvent) {
		this.foundEvent = foundEvent;
	}

	public List<Evenement> getFoundByType() {
		return foundByType;
	}

	public void setFoundByType(List<Evenement> foundByType) {
		this.foundByType = foundByType;
	}

	public List<Evenement> getFoundByNomUtili() {
		return foundByNomUtili;
	}

	public void setFoundByNomUtili(List<Evenement> foundByNomUtili) {
		this.foundByNomUtili = foundByNomUtili;
	}

	public Part getFile() {
		return file;
	}

	public void setFile(Part file) {
		this.file = file;
	}

	public String getMotRecherche() {
		return motRecherche;
	}

	public void setMotRecherche(String motRecherchee) {
		motRecherche = motRecherchee;
	}




}
