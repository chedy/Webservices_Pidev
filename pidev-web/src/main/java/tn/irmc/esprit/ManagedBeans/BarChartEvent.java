package tn.irmc.esprit.ManagedBeans;

import javax.annotation.PostConstruct;
import javax.ejb.EJB;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.RequestScoped;

import org.primefaces.model.chart.Axis;
import org.primefaces.model.chart.AxisType;
import org.primefaces.model.chart.BarChartModel;
import org.primefaces.model.chart.ChartSeries;

import tn.irmc.esprit.entities.Evenement;
import tn.irmc.esprit.entities.TypeOfEvents;
import tn.irmc.esprit.services.EvenementServiceLocal;

@ManagedBean
@RequestScoped
public class BarChartEvent {
	
	
	
	private BarChartModel barModel;
	@EJB
	private EvenementServiceLocal evenementServiceLocal;
 
    @PostConstruct
    public void init() {
    	Evenement e = new Evenement();
    	
        createBarModels();
    }
 
    public BarChartModel getBarModel() {
        return barModel;
    }
     
   /* public int max(){
    	int max=0;
    	max=ss.groupedByTheme("histoire");
    	
    	
    	if (max<ss.groupedByTheme("science humaine"))
    		max = ss.groupedByTheme("science humaine");
    	else if (max<ss.groupedByTheme("psychologie"))
    	    max = ss.groupedByTheme("psychologie");
    	else if (max<ss.groupedByTheme("sociale"))
    		max = ss.groupedByTheme("sociale");
    	return max;
    }*/
 
    private BarChartModel initBarModel() {
        BarChartModel model = new BarChartModel();

        ChartSeries his = new ChartSeries();
        his.setLabel("type");
        his.set("seminaire",evenementServiceLocal.groupedByTheme(TypeOfEvents.Seminaire));
        his.set("colloque",evenementServiceLocal.groupedByTheme(TypeOfEvents.Colloque));
        his.set("journee_etude",evenementServiceLocal.groupedByTheme(TypeOfEvents.Journee_etude));
        his.set("cycle_de_conferences",evenementServiceLocal.groupedByTheme(TypeOfEvents.Cycle_de_conferences));

        model.addSeries(his);

         
        return model;
    	
    }
     
    private void createBarModels() {
        createBarModel();
    }
     
    private void createBarModel() {
        barModel = initBarModel();
         
        barModel.setTitle("nombre de sequence par thematique");
        barModel.setLegendPosition("ne");
         
        Axis xAxis = barModel.getAxis(AxisType.X);
        xAxis.setLabel("thematique");
         
        Axis yAxis = barModel.getAxis(AxisType.Y);
        yAxis.setLabel("nombre de sequences");
        yAxis.setMin(0);
        yAxis.setMax(5);
    }

}