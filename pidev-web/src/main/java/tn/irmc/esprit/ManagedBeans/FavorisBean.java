package tn.irmc.esprit.ManagedBeans;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.annotation.PostConstruct;
import javax.ejb.EJB;
import javax.faces.bean.ManagedBean;

import tn.irmc.esprit.entities.Favoris;
import tn.irmc.esprit.entities.Offre;
import tn.irmc.esprit.entities.Reclamation;
import tn.irmc.esprit.services.FavorisService;
import tn.irmc.esprit.services.FavorisServiceLocal;
import tn.irmc.esprit.services.OfferService;
import tn.irmc.esprit.services.OfferServiceLocal;
import tn.irmc.esprit.services.UserServiceLocal;

@ManagedBean(name="FavBean")
public class FavorisBean {

	@EJB
	private FavorisServiceLocal favService;
	@EJB
	private OfferServiceLocal OffService;
	@EJB
	private UserServiceLocal userServ;

	private Favoris fav;
	private Offre off;
	private boolean marked;

	private static int idoff;
	private static int iduser;
	private List<Favoris> Favoris = new ArrayList<>();

	public Favoris getFav() {
		return fav;
	}

	public void setFav(Favoris fav) {
		this.fav = fav;
	}

	public Offre getOff() {
		return off;
	}

	public void setOff(Offre off) {
		this.off = off;
	}

	public static int getIdoff() {
		return idoff;
	}

	public static void setIdoff(int idoff) {
		FavorisBean.idoff = idoff;
	}

	public static int getIduser() {
		return iduser;
	}

	public static void setIduser(int iduser) {
		FavorisBean.iduser = iduser;
	}

	public List<Favoris> getFavoris() {
		return Favoris;
	}

	public void setFavoris(List<Favoris> favoris) {
		Favoris = favoris;
	}
	

	public boolean isMarked() {
		return marked;
	}

	public void setMarked(boolean marked) {
		this.marked = marked;
	}

	@PostConstruct
	public void init() {
		off = new Offre();
		fav = new Favoris();
		Favoris = favService.findAllFav();
	}

	public void doMarkOffre() {

		this.fav.setUser(userServ.findById(AuthenticationBean.getLoggedUser().CurrentUserId));
		this.fav.setOffre(OffService.findById(proposeOfferBean.id));
		favService.MarkFav(this.fav);
		marked=true;

	}
	public void doUnmarkFavOffre(){
		favService.UnmarkFav(this.fav);
		marked=false;
	}

}
