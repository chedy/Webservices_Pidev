package tn.irmc.esprit.ManagedBeans;

import java.io.IOException;
import java.io.Serializable;
import java.util.List;

import javax.annotation.PostConstruct;
import javax.ejb.EJB;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.RequestScoped;
import javax.servlet.http.Part;

import tn.irmc.esprit.entities.Role;
import tn.irmc.esprit.entities.User;
import tn.irmc.esprit.services.UserServiceLocal;

@ManagedBean(name = "profBean")
@RequestScoped
public class ProfileBean implements Serializable {
	@EJB
	private UserServiceLocal userLocal;
	private User user;
	private List<User> users;
	private boolean showMore = false;
	private User ThisUser;
	private Part image;
	
	

	@PostConstruct
	public void Init() {
		user = new User();
		users = userLocal.findAllUsers();
		ThisUser= userLocal.findById(AuthenticationBean.getLoggedUser().CurrentUserId);

	}

	// Getters And Setters
	public List<User> getUsers() {
		return users;
	}

	
	public Part getImage() {
		return image;
	}

	public void setImage(Part image) {
		this.image = image;
	}

	public void setUsers(List<User> users) {
		this.users = users;
	}

	public User getUser() {
		return user;
	}

	public void setUser(User user) {
		this.user = user;
	}
	public boolean isShowMore() {
		return showMore;
	}

	public void setShowMore(boolean showMore) {
		this.showMore = showMore;
	}

	public User getThisUser() {
		return ThisUser;
	}

	public void setThisUser(User thisUser) {
		ThisUser = thisUser;
	}
	
	// Methods
	
		
	public String doShow() {
		String navigateTo=null;
		showMore = true;
		navigateTo="/pages/User/EditProfile?faces-redirect=true";
		return navigateTo;
	}
		

	public String DoEdit() throws IOException {
		String navigateTo=null;
		
		image.write("c:\\Users\\chedi\\"+image.getSubmittedFileName());
		this.user.setId(AuthenticationBean.getLoggedUser().CurrentUserId);
		this.user.setLogin(user.getLogin());
		this.user.setEmail(user.getEmail());
		this.user.setAddress(user.getAddress());
		this.user.setCity(user.getCity());
		this.user.setCountry(user.getCountry());
		this.user.setFirstName(user.getFirstName());
		this.user.setLastName(user.getLastName());
		this.user.setPhoneNumber(user.getPhoneNumber());
		this.user.setPhoto("c:\\Users\\chedi\\"+image.getSubmittedFileName());
		this.user.setRole(Role.User);
		this.userLocal.UpdateUser(this.user);
		navigateTo="/pages/admin/Dashboard?faces-redirect=true";
		return navigateTo;
	}
	
	public String doDelete(){
		String navTo = "/pages/admin/Dashboard?faces-redirect=true";
		userLocal.DeleteUser(user);
		
		Init();
		return navTo;
	}

}
