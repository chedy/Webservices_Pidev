package tn.irmc.esprit.filters;

import java.io.IOException;


import javax.servlet.Filter;
import javax.servlet.FilterChain;
import javax.servlet.FilterConfig;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.annotation.WebFilter;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import tn.irmc.esprit.ManagedBeans.AuthenticationBean;



@WebFilter("/pages/admin/*")
public class AuthFilter implements Filter {

	@Override
	public void init(FilterConfig filterConfig) throws ServletException {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void doFilter(ServletRequest request, ServletResponse response, FilterChain chain)
			throws IOException, ServletException {
		
		HttpServletRequest req = (HttpServletRequest) request;
		HttpServletResponse resp = (HttpServletResponse) response;
		AuthenticationBean authBean = (AuthenticationBean) req.getSession()
				.getAttribute("authBean");

		boolean authorized = false;

		if ((authBean != null) && (authBean.isLoggedIn())&& (authBean.hasRole("Admin")))
		{
			authorized = true;
		}

		if (authorized) {
			chain.doFilter(request, response);
		} else {
			resp.sendRedirect(req.getContextPath() + "/pages/User/login.jsf");
		}
		
	}

	@Override
	public void destroy() {
		// TODO Auto-generated method stub
		
	}

	
	
	

}
